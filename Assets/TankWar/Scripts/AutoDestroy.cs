﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

	public float waitTime = 10;

	// Use this for initialization
	void Start () {

		Destroy(gameObject, waitTime);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

