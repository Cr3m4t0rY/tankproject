﻿using UnityEngine;
using System.Collections;

public class RotateTorret : MonoBehaviour {

	public float rotateSpeed = 10.0f;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//Torret Rotate

		if (Input.GetKey (KeyCode.A)) {

			transform.Rotate ( 0, 0, -rotateSpeed * Time.deltaTime);

		}

		if (Input.GetKey ((KeyCode.D))) {

			transform.Rotate ( 0, 0, rotateSpeed * Time.deltaTime);

		}
			
	}
}
