﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    private int level = 1;                                  //Current level number, expressed in game as "Day 1".
    public int dianas = 0;
    public int puntos = 0;
    public float tiempoRestante;
    private GameObject DianasHUD;
    private GameObject NivelHUD;
    private GameObject PuntosHUD;

    //Awake is always called before any Start functions
    void Awake() {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

    }

    void Start() {
        DianasHUD = GameObject.FindWithTag("HUDDianas");
        NivelHUD = GameObject.FindWithTag("HUDNivel");
        PuntosHUD = GameObject.FindWithTag("HUDPuntos");
        tiempoRestante = 122 - (level * 2);
    }

    //Update is called every frame.
    void Update() {
        contadorTiempo();
    }

    public void jugar() {
        SceneManager.LoadScene("Level01");
        NivelHUD.GetComponent<Text>().text = level.ToString();
    }

    private void actualizaHUD() {
        DianasHUD.GetComponent<Text>().text = dianas.ToString();
        PuntosHUD.GetComponent<Text>().text = puntos.ToString();
    }

    public void blanco() {
        dianas += 1;
        puntos += level * 10;
        actualizaHUD();
    }

    public int getLevel() {
        return (level);
    }

    public void contadorTiempo() {
        tiempoRestante -= Time.deltaTime;
        if (tiempoRestante < 0) {
            //GameOver();
        }
    }

}
