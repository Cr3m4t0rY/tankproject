﻿using UnityEngine;
using System.Collections;

public class TankControl : MonoBehaviour {

	public float forwardSpeed = 1.0f;
	public float backSpeed = 0.25f;
	public float rotateSpeed = 0.15f;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


		// Tank movement
	
		if (Input.GetKey (KeyCode.UpArrow)) {

			transform.Translate ( Vector3.forward * forwardSpeed * Time.deltaTime );

		}

		if (Input.GetKey ((KeyCode.DownArrow))) {

			transform.Translate ( Vector3.back * forwardSpeed * Time.deltaTime );

		}
		if (Input.GetKey (KeyCode.LeftArrow)) {

			transform.Rotate ( 0, -rotateSpeed * Time.deltaTime, 0 );

		}

		if (Input.GetKey ((KeyCode.RightArrow))) {

			transform.Rotate ( 0, rotateSpeed * Time.deltaTime, 0 );

		}




	}
}
