﻿using UnityEngine;
using System.Collections;

public class DianaSpawner : MonoBehaviour {

    int cajas = 0; 

    // Use this for initialization
    void Start () {
        int i;
        int range = GameManager.instance.getLevel() * 50;
        cajas = 20 - GameManager.instance.getLevel();
        for (i = 0; i <= cajas; i++) {
            GameObject diana = Instantiate(
                Resources.Load("Prefabs/DianaPrefab"),
                new Vector3(280 + Random.Range(-range, range), 0.5f, 61 + Random.Range(-range, range)),
                transform.rotation
            ) as GameObject;
        }
        Destroy(gameObject);

    }

    // Update is called once per frame
    void Update() {
    }        
}
